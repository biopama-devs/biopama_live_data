var regionSummary = {};
var countrySummary = {};//#of applications, total budget

/*

Map Layer addresses and other URL's unique to the Live Data Module
 
*/

var reverseProxyURL = 'https://proxy.biopama.org/'; //Reverse Proxy test proxy URL https://cors-anywhere.herokuapp.com/
var layerURLFloods = 'http://globalfloods-ows.ecmwf.int/glofas-ows/ows.py';  //Floods
var layerWMSName = 'FloodHazard100y';
var layerURLSSTrend = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/sst_trend/sst_trend_gm_tiles/{z}/{x}/{y}.png' //Sea Surface Trend (SST)
var layerURLSSTA = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/ssta/ssta_gm_tiles/{z}/{x}/{y}.png'; //Sea Surface Temperature Anomaly (SSTA)
var layerURLCoralBleaching = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/hs/hs_gm_tiles/{z}/{x}/{y}.png'; //Coral Bleaching HotSpot
var layerURLCoralHeatStress = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/baa_max_running/baa_gm_tiles/{z}/{x}/{y}.png'; //Daily Coral Bleaching Heat Stress Alert Area
var layerURLSSTemp = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/sst/sst_gm_tiles/{z}/{x}/{y}.png'; //Sea Surface Temperature (SST)

(function ($, Drupal) {
  Drupal.behaviors.biopamaLiveTwo = {
    attach: function (context, settings) {
      $(window).once().on('load scroll', function () {

	$('.icon-choropleth.interactive').click(function() {//Only applicable to interactive layers
		var currentLayerSet = $(this).attr('class').split(' ')[0];
		var currentLayer = $(this).attr('class').split(' ')[1];
		if($(this).hasClass("selected")){
			removeLiveLayer(currentLayer);
			$(this).removeClass("selected");
			//check status of other legend items in choropleth
			if ($("."+currentLayerSet+".selected.icon-choropleth").length === 0) {
				//if none, reset the button
				$('.add-layer-button' + '.' + currentLayerSet).html('<i class="fas fa-plus"></i>');
			} else {
				//find the last item in the legend turned on, and enable that layer
				var nextLegend = $("."+currentLayerSet+".selected.icon-choropleth").last().attr('class').split(' ')[1];
				addLiveLayer(nextLegend);
			}
		} else {
			addLiveLayer(currentLayer);
			$(this).addClass("selected");
			$('.add-layer-button' + '.' + currentLayerSet).html('<i class="fas fa-minus"></i>');
		}
	});

	$('.add-layer-button').click(function() {
		var currentLayer = $(this).attr('class').split(' ')[0]; 
		if($(this).hasClass("selected")){
			$(this).html('<i class="fas fa-plus"></i>');
			removeLiveLayer(currentLayer);
			$("."+currentLayer).removeClass("selected");
			$( "img." + currentLayer).removeClass("layer-spinner");
			$( "#mini-loader-wrapper." + currentLayer).remove();
		} else {
			addLiveLayer(currentLayer);
			$(this).addClass("selected");
			$(this).html('<i class="fas fa-minus"></i>');
		}
	});
		
	function addLiveLayer(currentLayer){
		if (mymap.getLayer(currentLayer)){
			console.log("Layer already in the map!")//to fix choropleth logic
		} else {
			var layerURL = "";
			var layerScheme = "xyz";
			if (currentLayer == "fire"){
				$(".fires_1d").addClass("selected");
			} else {
				$("."+currentLayer).addClass("selected");
			}
			switch(currentLayer) {
			  case "news":
				addEMMNews();
				return;
				break;
			case "airnews":
				addAirNews();
				return;
				break;
			  case "fire":
			  case "fires_1d":
				$('.add-layer-button.fires_1d').html('<i class="fas fa-minus"></i>');
				$(".fires_1d, .fire.add-layer-button").addClass("selected");
				layerURL = reverseProxyURL + 'https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time='+moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
				break;
			  case "fires_7d":
				$('.add-layer-button.fires_1d').html('<i class="fas fa-minus"></i>');
				$(".fires_7d, .fires_1d, .fire.add-layer-button").addClass("selected");
				layerURL = reverseProxyURL + 'https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time='+moment().subtract(7, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD'); 
				break;
			  case "fires_30d":
				$('.add-layer-button.fires_1d').html('<i class="fas fa-minus"></i>');
				$(".fires_30d, .fires_7d, .fires_1d, .fire.add-layer-button").addClass("selected");
				layerURL = reverseProxyURL + 'https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time='+moment().subtract(30, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD'); 
				break;
			  case "fires_90d":
				$('.add-layer-button.fires_1d').html('<i class="fas fa-minus"></i>');
				$(".fires_90d, .fires_30d, .fires_7d, .fires_1d, .fire.add-layer-button").addClass("selected");
				layerURL = reverseProxyURL + 'https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time='+moment().subtract(90, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD'); 
				break;
			  case "floods": 
			    mymap.setLayoutProperty("world_mask", 'visibility', 'visible');
				layerURL = reverseProxyURL + layerURLFloods + '?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.3.0&request=GetMap&crs=EPSG:3857&transparent=true&width=256&height=256&layers='+ layerWMSName +'&zIndex=33&opacity=1&time='+moment().format('YYYY-MM-DD');
				break;
			  case "drought": 
				//updated URL
				//4326 projection not supported
				var edoDay = moment().format('DD');
				var edoMonth = moment().format('MM') - 1;
				if(edoDay <=10) {edoDay = '01'}else if(edoDay >=11 && edoDay <= 20){edoDay = '11'}else if(edoDay >=21 ){edoDay = '21'}else{}
				layerURL = reverseProxyURL + 'https://edo.jrc.ec.europa.eu/gdo/php/gis/mswms.php?bbox={bbox-epsg-3857}&map=gdo_w_mf&LAYERS=rdri_png%2Cgrid_1dd_rdri&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SELECTED_YEAR=2020&SELECTED_MONTH=04&SELECTED_TENDAYS=21&SRS=EPSG%3A900913';//+moment().format('YYYY') + '-04-' + edoDay;  
				break;
			  case "spi":
				//also an unsupported projection
				//will fill url when supported
				break;
				var layerURLSSTrend = 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/sst_trend/sst_trend_gm_tiles/{z}/{x}/{y}.png' //Sea Surface Trend (SST)
			  case "coral":
				layerURL = reverseProxyURL + layerURLCoralBleaching;
				layerScheme = "tms";
				break;
			  case "coral-hot":
				layerURL = reverseProxyURL + layerURLCoralHeatStress;
				layerScheme = "tms";
				break;
			  case "ssta":
				layerURL = reverseProxyURL + layerURLSSTA;
				layerScheme = "tms";
				break;
			  case "sstt":
				layerURL = reverseProxyURL + layerURLSSTemp;
				layerScheme = "tms";
				break;
			  case "coral_dhw":
				layerURL = reverseProxyURL + 'https://coralreefwatch.noaa.gov/data1/vs/google_maps/dhw/dhw_gm_tiles/{z}/{x}/{y}.png';
				layerScheme = "tms";
				break;
			  default:
				return;
				// code block
			}
			mymap.addLayer({
				'id': currentLayer,
				'type': 'raster',
				'source': {
					'id': currentLayer+'-source',
					'type': 'raster',
					'tiles': [layerURL],
					'tileSize': 256,
					'scheme': layerScheme,
				},
				'paint': {}
			}, '');
		}
	}
	function removeLiveLayer(currentLayer){
		var layerArray = [];
		switch(currentLayer) {
		  case "news":
		    layerArray = ["unclustered-point","cluster-count","clusters","emm-news"];
			break;	
			case "airnews":
				layerArray = ["airnews"];
				break;	
		  case "fire":
		    //$(".fires_1d, .fires_7d, .fires_30d, .fires_90d").removeClass("selected");
			layerArray = ["fire","fires_1d","fires_7d","fires_30d","fires_90d"]
			break;
		  case "floods":
		    mymap.setLayoutProperty("world_mask", 'visibility', 'none');
			layerArray = [currentLayer]
		    break;			
		  default:
		    layerArray = [currentLayer]
		    break;
		}
		$.each(layerArray, function(index, value) {
			if (mymap.getLayer(layerArray[index])){
				mymap.removeLayer(layerArray[index]);
			}
			if (mymap.getSource(layerArray[index])){
				mymap.removeSource(layerArray[index]); 
			}
		});
		
	}
	$( "div.fires_1d" ).tooltip({
			title: moment().format('MMMM D, YYYY'),
	});
	$( "div.fires_7d" ).tooltip({
			title: moment().subtract(7, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
	});
	$( "div.fires_30d" ).tooltip({
			title: moment().subtract(30, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
	});
	$( "div.fires_90d" ).tooltip({
			title: moment().subtract(90, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
	});

	var mymap = $().createMap('map-container');

	$().addMapControls(mymap, "fullScreen");
	$().addMapControls(mymap, "navigation");	

	mymap.on('load', function () {
		mymap.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addSource("BIOPAMA_Point", {
			"type": 'vector',
			"tiles": [mapPointHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		console.log('test')
		mymap.addLayer({
			"id": "wdpaBase",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
            "paint": {
				"fill-color": "#78c119",
				"fill-opacity":0.7
            }
		});
		
		mymap.addLayer({
			"id": "wdpaAcpPolyLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": biopamaGlobal.map.layers.paLabelsLayer,
			"minzoom": 5,
            "layout": {
                "text-field": ["to-string", ["get", "NAME"]],
                "text-size": 12,
                "text-font": [
                    "Arial Unicode MS Regular",
                    "Arial Unicode MS Regular"
                ]
            },
            "paint": {
                "text-halo-width": 2,
                "text-halo-blur": 2,
                "text-halo-color": "hsl(0, 0%, 100%)",
                "text-opacity": 1
            }
		}, '');
		mymap.addLayer({
			"id": "wdpaAcpPointLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": biopamaGlobal.map.layers.paPointLayer,
			"minzoom": 5,
            "layout": {
                "text-field": "{NAME}",
                "text-size": 12,
                "text-padding": 3,
				"text-offset": [0,-1]
            },
            "paint": {
                "text-color": "hsla(213, 49%, 13%, 0.95)",
                "text-halo-color": "hsla(0, 0%, 100%, .9)",
                "text-halo-width": 2,
                "text-halo-blur": 2
            }
		}, '');
		//mymap.setLayoutProperty("world_mask", 'visibility', 'visible');
		mymap.addSource('world_mask', {
		'type': 'geojson',
		'data': {
		'type': 'Feature',
		'geometry': {
		'type': 'Polygon',
			"coordinates": [
			  [
				[-180,-90],[180,-90],[180,90],[-180,90],[-180,-90]
			  ]
			]
		}
		}
		});
		mymap.addLayer({
			'id': 'world_mask',
			'type': 'fill',
			'source': 'world_mask',
			'layout': { 'visibility': 'none'},
			'paint': {
			'fill-color': '#000',
			'fill-opacity': 0.7
			}
		}, '');
		
	});
	mymap.on('sourcedata', function(data) {
	  var sourceImage;
	  switch(data.sourceId) {
		  case "news":
			return;
			break;	
		  case "fires_1d":
		  case "fires_7d":
		  case "fires_30d":
		  case "fires_90d":
			sourceImage = "fire";
			break;		
		  default:
			sourceImage = data.sourceId;
			break;
	  }
	  if (data.isSourceLoaded == false){
		//check if the image has already got the spinner in it
		if ($( "img.layer-spinner." + sourceImage).length == 0){
			//load the spinner
			$( "img." + sourceImage).addClass("layer-spinner");
			$("<div id='mini-loader-wrapper' class='"+sourceImage+"'><div id='mini-loader'></div></div>").insertBefore(".add-layer-button." + sourceImage);
		}
	  } else {
		  $( "img." + sourceImage).removeClass("layer-spinner");
		  $( "#mini-loader-wrapper." + sourceImage).remove();
	  }
	  //console.log(data.sourceId);
	});
	

	function addAirNews(){
	
		mymap.addSource("airnews", {
				"type": "geojson",
				"data": "https://opendata.arcgis.com/datasets/5c5ea2ff0f724e4b9c20026912ea9dbe_0.geojson"
		});

		mymap.addLayer({
		"id": "airnews",
		"type": "circle",
		"source": "airnews",
		"paint": {
				"circle-radius": 3,
				"circle-color": "#ff9900"
			}
		});
		mymap.on('click', 'airnews', function (e) {
			console.log(e.features)
			var popText = '';
			var coordinates = e.features[0].geometry.coordinates.slice();
			for (var key in e.features) {
			popText = popText + "<h5>" + e.features[key].properties.name + "</h5>" +
			"<div class='pop-news-municipality'>" + e.features[key].properties.municipality + "</div>"+
			"<hr>"+
			"<span class='news-desc'><p>Status:</p>" + e.features[key].properties.status + "</span>";
			}

			// Ensure that if the map is zoomed out such that multiple
			// copies of the feature are visible, the popup appears
			// over the copy being pointed to.
			while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
				coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
			}

			new mapboxgl.Popup()
				.setLngLat(coordinates)
				.setHTML(popText)
				.addTo(mymap);
		});
	};
	function addEMMNews(){
		var emmGeoJson = {
			"type": "FeatureCollection",
			"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
		};

		jQuery.ajax({
			url: "/news/news.rss",
			dataType: 'xml',
			success: function(d) {
				var emmArticles = [];
				jQuery(d).find("item").each(function(){
					var emmTitle = jQuery(this).find("title").text();
					var emmLink = jQuery(this).find("link").text();
					var emmGUID = jQuery(this).find("guid").text();
					var emmDate = jQuery(this).find("pubDate").text();
					var emmDescription = jQuery(this).find("description").text();
					var emmPoint = jQuery(this).find("georss\\:point").text();
					//take only the results that have points
					if (emmPoint.length > 2){
						var emmLoc = emmPoint.split(" ");
						var emmXcoord = parseInt(emmLoc[1]);
						var emmYcoord = parseInt(emmLoc[0]);
						emmCoords = [emmXcoord, emmYcoord, 0];
						var emmArticle = { "type": "Feature", "properties": { "id": emmGUID, "title": emmTitle, "link": emmLink, "description": emmDescription, "date": emmDate }, "geometry": { "type": "Point", "coordinates": emmCoords } }
						//console.log(emmArticle);
						emmArticles.push(emmArticle)
					}
				});
				emmGeoJson.features = emmArticles;
				mymap.addSource("emm-news", {
					type: "geojson",
					data: emmGeoJson,
					cluster: true,
					clusterMaxZoom: 10,
					clusterRadius: 30
				});
				mymap.addLayer({
					id: "clusters",
					type: "circle",
					source: "emm-news",
					filter: ["has", "point_count"],
					paint: {
						// Use step expressions (https://www.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
						// with three steps to implement three types of circles:
						//   * Blue, 20px circles when point count is less than 5
						//   * Yellow, 30px circles when point count is between 5 and 10
						//   * Pink, 40px circles when point count is greater than or equal to 10
						"circle-color": [
							"step",
							["get", "point_count"],
							"#1e90ff",
							5,
							"#1e90ff",
							10,
							"#1e90ff"
						],
						"circle-radius": [
							"step",
							["get", "point_count"],
							10,
							5,
							15,
							10,
							20
						]
					}
				});

				mymap.addLayer({
					id: "cluster-count",
					type: "symbol",
					source: "emm-news",
					filter: ["has", "point_count"],
					layout: {
						"text-field": "{point_count_abbreviated}",
						"text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
						"text-size": 12
					}
				});

				mymap.addLayer({
					id: "unclustered-point",
					type: "circle",
					source: "emm-news",
					filter: ["!", ["has", "point_count"]],
					paint: {
						"circle-color": "#1e90ff",
						"circle-radius": 6,
						"circle-stroke-width": 1,
						"circle-stroke-color": "#fff"
					}
				});

				// inspect a cluster on click
				mymap.on('click', 'clusters', function (e) {
					var features = mymap.queryRenderedFeatures(e.point, { layers: ['clusters'] });
					var clusterId = features[0].properties.cluster_id;
					mymap.getSource('emm-news').getClusterExpansionZoom(clusterId, function (err, zoom) {
						if (err)
							return;
						var curZoom = mymap.getZoom();
						
						if (curZoom == zoom){
							console.log(features)
						} else {
							mymap.easeTo({
								center: features[0].geometry.coordinates,
								zoom: zoom + 1
							});
						}
					});
				});
				mymap.on('click', 'unclustered-point', function (e) {
					console.log(e.features)
					var popText = '';
					var coordinates = e.features[0].geometry.coordinates.slice();
					for (var key in e.features) {
					popText = popText + "<h5>" + e.features[key].properties.title + "</h5>" +
							"<hr>" +
							"<span class='news-desc'>" + e.features[key].properties.description + "</span>" +
							"<hr>" +
							"<div class='pop-news-date'>" + e.features[key].properties.date + "</div>" +
							"<span class='news-link'><a href='" + e.features[key].properties.link + "' target='_blank' class='btn btn-info btn-sm' role='button'>Read More</a></span>";
					}

					// Ensure that if the map is zoomed out such that multiple
					// copies of the feature are visible, the popup appears
					// over the copy being pointed to.
					while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
						coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
					}

					new mapboxgl.Popup()
						.setLngLat(coordinates)
						.setHTML(popText)
						.addTo(mymap);
				});

				mymap.on('mouseenter', 'clusters', function () {
					mymap.getCanvas().style.cursor = 'pointer';
				});
				mymap.on('mouseleave', 'clusters', function () {
					mymap.getCanvas().style.cursor = '';
				});
				mymap.on('mouseenter', 'unclustered-point', function () {
					mymap.getCanvas().style.cursor = 'pointer';
				});
				mymap.on('mouseleave', 'unclustered-point', function () {
					mymap.getCanvas().style.cursor = '';
				});
				
			},
			error: function() {
				console.log("No news")
			}
		});	

	}
      });
    }
  };
})(jQuery, Drupal);
